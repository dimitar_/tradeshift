package si.dime.tradeshift.triangle;

import java.util.HashSet;

/**
 * Represents a single immutable triangle
 *
 * Created by dime on 5/24/17.
 */
public final class Triangle {
    // The sides of the triangle
    private final double a;
    private final double b;
    private final double c;

    // The type of the triangle
    private final Type type;

    /**
     * The default constructor
     *
     * @param a
     *      The triangle's A side
     * @param b
     *      The triangle's B side
     * @param c
     *      The triangle's C side
     */
    Triangle(double a, double b, double c) {
        // Save the sides
        this.a = a;
        this.b = b;
        this.c = c;

        // The sum of any 2 sides of a triangle must be
        // greater than the measure of the third side.
        if ((a + b > c) && (b + c > a) && (a + c > b)) {
            int uniqueSides = new HashSet<Double>() {{ add(a); add(b); add(c); }}.size();
            type = Type.resolveByUniqueSides(uniqueSides);
        } else {
            // A triangle cannot be created
            type = Type.INVALID;
        }
    }

    /**
     * Returns the type of this triangle.
     *
     * @return
     *      The triangle's type
     */
    Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("Triangle[a = %.2f, b = %.2f, c = %.2f, type = %s]",
                a, b, c, type.name().toLowerCase());
    }

    /**
     * All available triangle types
     */
    public enum Type {
        // If all of the 3 sides are equal
        EQUILATERAL,
        // If exactly 2 sides are equal
        ISOSCELES,
        // If all 3 sides are unequal
        SCALENE,
        // Invalid sides (cannot form a triangle)
        INVALID;

        /**
         * Returns the type of the triangle
         * based on the given number of unique sides
         *
         * @param uniqueSides
         *      The number of unique sides
         * @return
         *      The type of the triangle
         */
        private static Type resolveByUniqueSides(int uniqueSides) {
            switch (uniqueSides) {
                case 1:
                    return EQUILATERAL;
                case 2:
                    return ISOSCELES;
                case 3:
                    return SCALENE;
                default:
                    return INVALID;
            }
        }
    }
}
