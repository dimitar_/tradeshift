package si.dime.tradeshift.triangle;

/**
 * The main class
 *
 * Created by dime on 5/24/17.
 */
public class App {
    // The exit code returned on invalid input
    private static final int INVALID_INPUT_EXIT_CODE = 1;

    /**
     * Default constructor
     *
     * @param args
     *      Exactly 3 input parameters: the sides of the triangle: a, b and c
     */
    App(final String[] args) {
        // We must have exactly 3 sides provided
        if (args == null || args.length != 3) {
            printUsage();
            System.exit(INVALID_INPUT_EXIT_CODE);
        }

        // All of the sides must be valid doubles
        final Double a = parseSide(args[0]);
        final Double b = parseSide(args[1]);
        final Double c = parseSide(args[2]);

        // Sanity check
        if (a == null || b == null || c == null) {
            printUsage();
            System.exit(INVALID_INPUT_EXIT_CODE);
        }

        // Create the triangle and print it's description
        System.out.println(new Triangle(a, b, c));
    }

    /**
     * Tries to parse the given side to a double.
     * If it fails - returns null
     *
     * @param side
     *      The side of the triangle
     * @return
     *      The side of the triangle as Double
     *      Null - if the given string is not a double
     */
    private Double parseSide(final String side) {
        try {
            return Double.parseDouble(side);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Prints the usage of this app
     */
    private void printUsage() {
        System.out.println("Usage: app a b c");
        System.out.println("    a, b, c - the sides of the triangle (valid doubles)");
    }

    /**
     * The entry point.
     *
     * @param args
     *      Exactly 3 input parameters: the sides of the triangle: a, b and c
     */
    public static void main(String[] args) {
        new App(args);
    }
}
