package si.dime.tradeshift.triangle;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests for the Triangle
 *
 * Created by dime on 5/25/17.
 */
public class TriangleTest {
    // A set of different triangles
    private static final Triangle INVALID_INPUT = new Triangle(-1,0,8);
    private static final Triangle INVALID = new Triangle(10,4,6);
    private static final Triangle EQUILATERAL = new Triangle(7,7,7);
    private static final Triangle ISOSCELES = new Triangle(6,4,6);
    private static final Triangle SCALENE = new Triangle(5,10,6);

    @Test
    public void testAllTypes() {
        assertTrue(INVALID_INPUT.getType() == Triangle.Type.INVALID);
        assertTrue(INVALID.getType() == Triangle.Type.INVALID);
        assertTrue(EQUILATERAL.getType() == Triangle.Type.EQUILATERAL);
        assertTrue(ISOSCELES.getType() == Triangle.Type.ISOSCELES);
        assertTrue(SCALENE.getType() == Triangle.Type.SCALENE);
    }
}
