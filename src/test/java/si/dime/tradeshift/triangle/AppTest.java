package si.dime.tradeshift.triangle;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

/**
 * Tests for the App
 *
 * Created by dime on 5/25/17.
 */
public class AppTest {
    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void testNullInput() {
        exit.expectSystemExitWithStatus(1);
        new App(null);
    }

    @Test
    public void testIncompleteInput() {
        exit.expectSystemExitWithStatus(1);
        new App(new String[] {"", ""});
    }

    @Test
    public void testInvalidInput() {
        exit.expectSystemExitWithStatus(1);
        new App(new String[] {"invalid", "", "8.0"});
    }

    @Test
    public void testEmptyInput() {
        exit.expectSystemExitWithStatus(1);
        new App(new String[] {"", "", ""});
    }

    @Test
    public void testIntInput() {
        new App(new String[] {"1", "4", "8"});
    }

    @Test
    public void testValidInput() {
        new App(new String[] {"10.4", "5.9", "8.0"});
    }
}
